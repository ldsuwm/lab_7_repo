from django.apps import AppConfig


class Lab7AppConfig(AppConfig):
    name = 'lab_7_app'
